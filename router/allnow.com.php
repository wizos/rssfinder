<?php
function exe($url) {
    // 示例1：https://www.allnow.com/tag/118
    if (preg_match("/allnow\.com\/tag\/([^?]+?)(\?|\/|$)/s", $url, $temp)) {
        return "https://rsshub.app/allnow/tag/" . $temp[1];
    }
    // 示例2：https://www.allnow.com/column/119
    if (preg_match("/allnow\.com\/column\/([^?]+?)(\?|\/|$)/s", $url, $temp)) {
        return "https://rsshub.app/allnow/column/" . $temp[1];
    }
    // 示例3：https://www.allnow.com/user/1891141
    if (preg_match("/allnow\.com\/user\/([^?]+?)(\?|\/|$)/s", $url, $temp)) {
        return "https://rsshub.app/allnow/user/" . $temp[1];
    }
}